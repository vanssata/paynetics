<?php


namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\HttpFoundation\Request;


abstract class AbstractClientController extends AbstractController
{
    protected $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    abstract protected function storeToApi(FormInterface $form);

    public function getApiCollection(Request $request, string $endpoint){
        try {
            $params = $request->query->all();

            $response = $this->client->request('GET',  $_ENV['PROJECT_API_URL']. $endpoint, ['query' => $params]);
            $data = [];

            if ($response->getStatusCode() === 200 and $response->toArray()['code'] === 0) {
                $data = $response->toArray();
                if (isset($data['data']['links'])) {
                    foreach ($data['data']['links'] as $key => $link) {
                        if ($link) {
                            $data['data']['links'][$key] = parse_url($link)['query'];
                        }
                    }
                }
            }

            if(!isset($data['code'])){
                throw new \Exception('Something broke');
            }

            return $data;
        }catch (\Throwable $e){

            $this->addFlash('danger',$e->getMessage());
            return [
                'code'=>-1
            ];

        }
    }
}
