<?php


namespace App\Controller\API;


use App\Entity\Project;
use App\Form\ProjectApiFormType;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractBaseApiController extends AbstractController
{

    final public function handleJsonDataRequest(Request $request):?iterable
    {
        return \json_decode($request->getContent(), true);
    }

    public function renderJsonException(\Throwable $throwable,array $context = []){
        $response = [
            'code' => -1,
            'data' => $context,
            'error' => [
                'error_code' => $throwable->getCode(),
                'error_message' => $throwable->getMessage(),
            ]
        ];
        return $this->json($response,200);
    }

    public function renderJsonError($error,array $context = []){
        $response = [
            'code' => -1,
            'data' => $context,
            'error' => $error
        ];
        return $this->json($response,200);
    }

    public function returnJson($data, ?array $groups, ?array $errors = []): JsonResponse
    {
        if (!empty($groups)) {
            $groups[] = 'default';
        }

        $data_to_json = [
            'code' => empty($errors) ? 0 : -1,
            'data' => $data,
            'errors' => $errors,
        ];

        return $this->json($data_to_json, 200, [], [
            'enable_max_depth' => false,
            'groups' => $groups
        ]);
    }

}
