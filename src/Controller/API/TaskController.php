<?php


namespace App\Controller\API;


use App\Entity\Project;
use App\Entity\Task;
use App\Form\ErrorFormUtils;
use App\Form\TaskApiFormType;
use Nelmio\ApiDocBundle\Annotation\Model;

use App\Pagination\PaginationFactory;
use App\Repository\TaskRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TaskController
 * @package App\API\Controller
 * @Route(path="/api/task")
 */
class TaskController extends AbstractBaseApiController
{

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route(methods={"GET"}, name="api_task_list", path="")
     * @SWG\Response(
     *     response=200,
     *     description="Returns the resource responses response body <br> <ul> <li>code = 0 success </li> <li>code = -1 error</li> </ul>",
     *     @SWG\Schema(ref="#/definitions/TaskList")
     * )
     * @SWG\Parameter( name="page", in="query", type="string", description="Current page", default="0" )
     * @SWG\Tag(name="task")
     *
     * @IsGranted("IS_AUTHENTICATED_ANONYMOUSLY")
     */

    public function indexAction(Request $request, TaskRepository $taskRepository, PaginationFactory $paginationFactory)
    {
        try {
            $qb = $taskRepository->createApiListQueryBulder();

            return $this->returnJson($paginationFactory->createCollectionFromRequest($qb, $request, 'api_task_list'), ['list_task', 'default']);
        } catch (\Exception $e) {
            return $this->returnJson(null, null, ['errorCode' => $e->getCode(), $e->getMessage()]);
        }
    }

    /**
     * Show Task
     * @param Project $project
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route(methods={"GET"}, name="api_task_show", path="/{id}/")
     * @SWG\Response(
     *    @SWG\Schema(
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="data", ref=@Model(type=Task::class, groups={"show_task"} )  ),
     *             @SWG\Property(property="error", type="object", example={{"message": "Not Found"}}  )
     *      ),
     *     response=200,
     *     description="Returns the resource responses response body <br> <ul> <li>code = 0 success </li> <li>code = -1 error</li> </ul>",
     * )
     * )
     * @SWG\Tag(name="task")
     */
    public function showAction(string $id, TaskRepository $taskRepository)
    {
        $task = $taskRepository->find($id);
        if ($task instanceof Task) {
            return $this->returnJson($task, ['show_task']);
        }else{
            return $this->renderJsonError('Resource Not Found');
        }
        return $this->renderJsonError(['Not Found']);
    }

    /**
     * Update Task
     * @Route(methods={"PUT"}, name="api_project_update", path="/{id}",requirements={"id":"\d+"})
     * @SWG\Parameter( name="form", in="body", format="json", description="Create a new Project", @Model(type=TaskApiFormType::class) )
     * @SWG\Response(
     *    @SWG\Schema(
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="data", type="object",example={"Success"}  ),
     *             @SWG\Property(property="error", type="object", example={{"message": "Not Found"}}  )
     *      ),
     *     response=200,
     *     description="Task Update <br> <ul> <li>code = 0 success </li> <li>code = -1 error</li> </ul>",
     * )
     * )
     * @SWG\Tag(name="task")
     */
    public function updateAction(Request $request, TaskRepository $taskRepository)
    {
        try {
            $task = $taskRepository->find($request->get('id'));

            $form = $this->createForm(TaskApiFormType::class, $task);
            $form->submit($this->handleJsonDataRequest($request));

            if (!$form->isValid()) {
                return $this->returnJson(null, null, ErrorFormUtils::generateErrorsArrayFromForm($form));
            }

            $taskRepository->store($form->getData());

        } catch (\Exception $e) {
            return $this->returnJson($e->getMessage(), null, $e->getTrace());
        }

        return $this->returnJson('Success', ['show_project']);
    }

    /**
     * Create Task
     * @Route(methods={"POST"}, name="api_task_create", path="/")
     * @SWG\Parameter( name="form", in="body", format="json", description="Create a new Project", @Model(type=TaskApiFormType::class) )
     * @SWG\Response(
     *    @SWG\Schema(
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="data", type="object",example={"Success"}  ),
     *             @SWG\Property(property="error", type="object", example={{"message": "Not Found"}}  )
     *      ),
     *     response=200,
     *  description="Create Task Resource <br> <ul> <li>code = 0 success </li> <li>code = -1 error</li> </ul>",
     * )
     * )
     * @SWG\Tag(name="task")
     */
    public function createAction(Request $request, TaskRepository $taskRepository): \Symfony\Component\HttpFoundation\JsonResponse
    {
        try {
            $form = $this->createForm(TaskApiFormType::class, new Task());
            $form->submit($this->handleJsonDataRequest($request));

            if (!$form->isValid()) {
                return $this->returnJson(null, null, ErrorFormUtils::generateErrorsArrayFromForm($form));
            }

            $taskRepository->store($form->getData());

        } catch (\Exception $e) {
            return $this->returnJson($e->getMessage(), null, $e->getTrace());
        }


        return $this->returnJson('Success', ['show_project']);
    }

    /**
     * Delete Task
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route(methods={"DELETE"}, name="api_task_delete", path="/{id}/")
     * @SWG\Response(
     *    @SWG\Schema(
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="data", type="object",example={"Success delete project"}  ),
     *             @SWG\Property(property="error", type="object", example={{"message": "Not Found"}}  )
     *      ),
     *     response=200,
     *     description="Delete Task Resource  <br> <ul> <li>code = 0 success </li> <li>code = -1 error</li> </ul>",
     * )
     * )
     * @SWG\Tag(name="task")
     */
    public function deleteAction(string $id, TaskRepository $taskRepository)
    {
        try {
            $taskRepository->deleteById($id);
            return $this->returnJson('Success delete task ' . $id, []);
        } catch (\Exception $e) {
            return $this->renderJsonException($e);
        }

    }


}
