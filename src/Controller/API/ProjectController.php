<?php


namespace App\Controller\API;


use App\Entity\Project;
use App\Form\ErrorFormUtils;
use App\Form\ProjectApiFormType;
use App\Pagination\PaginatedCollection;
use App\Pagination\PaginationFactory;
use App\Repository\ProjectRepository;


use App\Controller\API\ProjectResoponseDocumentatorDTO;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ProjectController
 * @package App\API\Controller
 * @Route(path="/api/project")
 */
class ProjectController extends AbstractBaseApiController
{

    /**
     * List the Projects.
     * @SWG\Response(
     *     response=200,
     *     description="Returns the resource responses response body <br> <ul> <li>code = 0 success </li> <li>code = -1 error</li> </ul>",
     *     @SWG\Schema(ref="#/definitions/ProjectList")
     * )
     * @SWG\Parameter( name="page", in="query", type="string", description="Current page", default="0" )
     * @SWG\Tag(name="project")
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route(methods={"GET"}, name="project_list", path="/")
     */
    public function indexAction(Request $request, ProjectRepository $projectRepository, PaginationFactory $paginationFactory)
    {
        try {
            $qb = $projectRepository->createApiListQueryBulder();
            return $this->returnJson($paginationFactory->createCollectionFromRequest($qb, $request, 'project_list'), ['list_project', 'default']);
        } catch (\Throwable $e) {
            return $this->renderJsonException($e);
        }
    }

    /**
     * Current Project.
     * @param Project $project
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route(methods={"GET"}, name="api_show", path="/{id}",requirements={"id":"\d+"})
     * @SWG\Response(
     *    @SWG\Schema(
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="data", ref=@Model(type=Project::class, groups={"show_project"} )  ),
     *             @SWG\Property(property="error", type="object", example={{"message": "Not Found"}}  )
     *      ),
     *     response=200,
     *     description="Returns the resource responses response body <br> <ul> <li>code = 0 success </li> <li>code = -1 error</li> </ul>",
     * )
     * )
     * @SWG\Tag(name="project")
     */


    public function showAction(int $id, ProjectRepository $projectRepository)
    {
        try {
            $project = $projectRepository->find($id);
            if (!$project) {
                return $this->renderJsonError('Resource Not Found');
            }
            return $this->returnJson($project, ['show_project']);
        } catch (\Throwable $e) {
            return $this->renderJsonException($e);
        }
    }

    /**
     * Update Project
     * @Route(methods={"PUT"}, name="api_project_update", path="/{id}",requirements={"id":"\d+"})
     * @SWG\Parameter( name="form", in="body", format="json", description="Create a new Project", @Model(type=ProjectApiFormType::class) )
     * @SWG\Response(
     *    @SWG\Schema(
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="data", type="object",example={"Success"}  ),
     *             @SWG\Property(property="error", type="object", example={{"message": "Not Found"}}  )
     *      ),
     *     response=200,
     *     description="Update Project <br> <ul> <li>code = 0 success </li> <li>code = -1 error</li> </ul>",
     * )
     * )
     * @SWG\Tag(name="project")
     */
    public function updateAction($id,Request $request, ProjectRepository $projectRepository, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        try {
            $project = $projectRepository->find($id);
            $form = $this->createForm(ProjectApiFormType::class, $project);
            $form->submit($this->handleJsonDataRequest($request));

            if (!$form->isValid()) {
                return $this->renderJsonError(ErrorFormUtils::generateErrorsArrayFromForm($form));
            }

            $projectRepository->store($form->getData());

            return $this->returnJson('Success', ['show_project']);
        } catch (\Throwable $e) {
            return $this->renderJsonException($e);
        }

    }

    /**
     * Create Project
     * @Route(methods={"POST"}, name="api_project_create", path="/")
     * @SWG\Parameter( name="form", in="body", format="json", description="Create a new Project", @Model(type=ProjectApiFormType::class) )
     * @SWG\Response(
     *    @SWG\Schema(
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="data", type="object",example={"Success"}  ),
     *             @SWG\Property(property="error", type="object", example={{"message": "Not Found"}}  )
     *      ),
     *     response=200,
     *  description="Create Project Resource <br> <ul> <li>code = 0 success </li> <li>code = -1 error</li> </ul>",
     * )
     * )
     * @SWG\Tag(name="project")
     */
    public function createAction(Request $request, ProjectRepository $projectRepository, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        try {
            $form = $this->createForm(ProjectApiFormType::class, new Project());
            $form->submit($this->handleJsonDataRequest($request));

            if (!$form->isValid()) {
                return $this->renderJsonError(ErrorFormUtils::generateErrorsArrayFromForm($form));
            }

            $projectRepository->store($form->getData());
            return $this->returnJson('Success', ['show_project']);
        } catch (\Throwable $e) {
            return $this->renderJsonException($e);
        }
    }


    /**
     * Delete Project
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route(methods={"DELETE"}, name="api_project_delete", path="/{id}/")
     * @SWG\Response(
     *    @SWG\Schema(
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="data", type="object",example={"Success delete project"}  ),
     *             @SWG\Property(property="error", type="object", example={{"message": "Not Found"}}  )
     *      ),
     *     response=200,
     *     description="Delete Project Resource  <br> <ul> <li>code = 0 success </li> <li>code = -1 error</li> </ul>",
     * )
     * )
     * @SWG\Tag(name="project")
     */
    public function deleteAction(string $id, ProjectRepository $projectRepository)
    {
        try {
            $projectRepository->deleteById($id);
            return $this->returnJson('Success delete project  ' . $id, []);
        } catch (\Exception $e) {
            return $this->renderJsonException($e);
        }

    }


}
