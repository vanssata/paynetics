<?php

namespace App\Controller;

use App\Entity\Task;
use App\Form\TaskApiFormType;
use App\Form\TaskType;
use App\Repository\TaskRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * @Route("/task")
 */
class TaskController extends AbstractClientController
{

    /**
     * @Route("/", name="task_index", methods={"GET"})
     */
    public function index(Request $request): Response
    {
        return $this->render('task/index.html.twig', [
            'tasks' => $this->getApiCollection($request,'task'),
        ]);
    }

    /**
     * @Route("/new", name="task_new", methods={"GET","POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function new(Request $request): Response
    {
        $task = new Task();
        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->storeToApi($form)) {
                return $this->redirectToRoute('task_index');
            }
        }

        return $this->render('task/new.html.twig', [
            'task' => $task,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="task_show", methods={"GET"})
     */
    public function show($id): Response
    {
        $response = $this->client->request('GET', $_ENV['PROJECT_API_URL'].'task/' . $id);
        $task = [];
        if ($response->getStatusCode() === 200 and $response->toArray(false)['code'] === 0) {
            $task = $response->toArray(false);
        }else{
            throw new NotFoundHttpException();
        }

        return $this->render('task/show.html.twig', [
            'task' => $task['data'],
        ]);
    }

    /**
     * @Route("/{id}/edit", name="task_edit", methods={"GET","POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function edit(Request $request, Task $task): Response
    {
        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->storeToApi($form)) {
                return $this->redirectToRoute('task_show', ['id' => $task->getId()]);
            }
        }

        return $this->render('task/edit.html.twig', [
            'task' => $task,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="task_delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(Request $request, Task $task): Response
    {
        if ($this->isCsrfTokenValid('delete' . $task->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($task);
            $entityManager->flush();
        }

        return $this->redirectToRoute('task_index');
    }

    protected function storeToApi(FormInterface $form)
    {
        $data = $form->getData();
        $id = $data->getId() ?? null;

        $to_send = [
            "title" => $data->getTitle(),
            "description" => $data->getDescription(),
            "status" => $data->getStatus(),
            "duration" => $data->getDuration()->format('Y-m-d\TH:i:sO'),
            "project" => $data->getProject()->getId(),
        ];
        try {
            $response = $this->client->request('PUT', $_ENV['PROJECT_API_URL'].'task/' . $id, ['json' => $to_send]);

            if ($response->toArray(false)['code'] === 0) {
                $this->addFlash('success', 'Successful add task ' . $data->getTitle() . "!!!");
                return true;
            } else {
                $this->addFlash('danger', 'Something broke in API, please try again');
            }
        } catch (\Throwable $e) {
            $this->addFlash('danger', $e->getMessage());
        }
        return false;
    }
}
