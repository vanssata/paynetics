<?php

namespace App\Form;

use App\Entity\StatusesInterface;
use App\Entity\Task;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;

class TaskApiFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'property_path' => 'title'
            ])

            ->add('description', TextareaType::class, [
                'property_path' => 'description'
            ])
            ->add('status', ChoiceType::class, [
                'choices' => [
                    StatusesInterface::STATUS_NEW => StatusesInterface::STATUS_NEW,
                    StatusesInterface::STATUS_PENDING => StatusesInterface::STATUS_PENDING,
                    StatusesInterface::STATUS_FAILED => StatusesInterface::STATUS_FAILED,
                    StatusesInterface::STATUS_DONE => StatusesInterface::STATUS_DONE,
                ],
                'property_path' => 'status',
                'constraints' => [
                    new Choice(['choices' => [
                        StatusesInterface::STATUS_NEW,
                        StatusesInterface::STATUS_PENDING,
                        StatusesInterface::STATUS_FAILED,
                        StatusesInterface::STATUS_DONE,
                    ]])
                ]
            ])
            ->add('duration',DateType::class,[
                'property_path' => 'duration',
                'html5' => true,
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'form-control input-inline datetimepicker',
                    'data-provide' => 'datetimepicker',
                    'html5' => false,
                ],

            ])
            ->add('project');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Task::class,
            'csrf_protection' => false,
        ]);
    }
}
