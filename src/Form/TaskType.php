<?php

namespace App\Form;

use App\Entity\StatusesInterface;
use App\Entity\Task;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TaskType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('description')
            ->add('status',ChoiceType::class, [
                'choices' => [
                    StatusesInterface::STATUS_NEW => StatusesInterface::STATUS_NEW,
                    StatusesInterface::STATUS_PENDING => StatusesInterface::STATUS_PENDING,
                    StatusesInterface::STATUS_FAILED => StatusesInterface::STATUS_FAILED,
                    StatusesInterface::STATUS_DONE => StatusesInterface::STATUS_DONE,
                ]])
            ->add('duration',DateType::class,[
                'html5' => true,
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'form-control input-inline datetimepicker',
                    'data-provide' => 'datetimepicker',
                    'html5' => false,
                ],

            ])
            ->add('project')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Task::class,
        ]);
    }
}
