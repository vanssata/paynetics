<?php
declare(strict_types=1);

namespace App\Entity;

use App\Repository\ProjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Faker\Provider\DateTime;
use Gedmo\Mapping\Annotation as Gedmo;
use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass=ProjectRepository::class)
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)

 */
class Project implements StatusesInterface
{
    use BaseFieldsTrait;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"show_project", "list_project","show_task"})
     */
    private $id;
    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=255)
     * @Groups({"show_project", "list_project","show_task"})
     */
    private $title;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="text")
     * @Groups({"show_project"})
     */
    private $description;


    /**
     * @ORM\OneToMany(targetEntity=Task::class, mappedBy="project")
     * @Groups({"show_project"})
     */
    private $task;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"show_project", "list_project"})
     */
    private $client;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"show_project", "list_project"})
     */
    private $company;


    public function __construct()
    {
        $this->task = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @Groups({"show_project", "list_project"})
     * @return \DateTimeInterface|null
     */
    public function getDuration(): ?\DateTime
    {

        $criteria = Criteria::create()
            ->andWhere(Criteria::expr()->neq('duration',null))
            ->orderBy(array("duration" => Criteria::DESC));

        $max_task = $this->task->matching($criteria)->first();

        if ($max_task instanceof Task) {

            if($max_task->getDuration() instanceof \DateTime){
                return $max_task->getDuration();
            }
        }
        return null;

    }


    /**
     * @return Collection|Task[]
     */
    public function getTask(): Collection
    {
        return $this->task;
    }

    public function addTask(Task $task): self
    {
        if (!$this->task->contains($task)) {
            $this->task[] = $task;
            $task->setProject($this);
        }

        return $this;
    }

    public function removeTask(Task $task): self
    {
        if ($this->task->removeElement($task)) {
            // set the owning side to null (unless already changed)
            if ($task->getProject() === $this) {
                $task->setProject(null);
            }
        }

        return $this;
    }

    public function getClient(): ?string
    {
        return $this->client;
    }

    public function setClient(?string $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(?string $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @SWG\Property(type="string",default=" ")
     * @Groups({"show_project", "list_project"})
     * @return mixed
     */
    public function getStatus():string
    {
        $status = array_unique($this->task->map(function (Task $task) {
            return $task->getStatus();
        })->toArray());

        if (count($status) == 0){
            return self::STATUS_NEW;
        }elseif(count($status) == 1){
         return $status[0];
        }else{
            return self::STATUS_PENDING;
        }
    }


    /**
     * @Assert\Callback
     */
    public function validateCompanyOrClient(ExecutionContextInterface $context, $payload)
    {
        if ($this->getClient() === null and $this->getCompany() === null) {

            $context->buildViolation('Please enter company or client')
                ->atPath('company')->addViolation();

            $context->buildViolation('Please enter company or client')
                ->atPath('client')
                ->addViolation();
        }

    }




    public function __toString(): string
    {
        return (string)$this->getTitle();
    }

}
