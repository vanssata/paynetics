<?php
declare(strict_types=1);

namespace App\Entity;

/**
 * Interface StatusesInterface
 * @package App\Entity
 */
interface StatusesInterface
{
    public const STATUS_NEW = 'new';
    public const STATUS_PENDING = 'pending';
    public const STATUS_FAILED = 'failed';
    public const STATUS_DONE = 'done';


}
