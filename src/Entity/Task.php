<?php
declare(strict_types=1);

namespace App\Entity;

use App\Repository\TaskRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=TaskRepository::class)
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Task
{
    use BaseFieldsTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"show_task", "list_task","show_project"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"show_task", "list_task", "show_project"})
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Groups({"show_task"})
     */
    private $description;

    /**
     * @ORM\Column(type="string")
     * @Groups({"show_task", "list_task", "show_project"})
     */
    private $status = StatusesInterface::STATUS_NEW;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"show_task", "list_task", "show_project"})
     */
    private $duration;

    /**
     * @ORM\ManyToOne(targetEntity=Project::class, inversedBy="task")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"show_task"})
     */
    private $project;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getDuration(): ?\DateTimeInterface
    {
        return $this->duration;
    }

    public function setDuration(?\DateTimeInterface $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    public function __toString():string
    {
        return (string) $this->getTitle();
    }
}
