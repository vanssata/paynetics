<?php

namespace App\DataFixtures;

use App\Entity\Project;
use App\Entity\StatusesInterface;
use App\Entity\Task;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProjectFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();
        for ($i = 0; $i <= $faker->numberBetween(10, 100000); $i++) {
            $project = new Project();
            if ($faker->boolean(50)) {
                $project->setClient($faker->userName);
            } else {
                $project->setCompany($faker->company);
            }
            $project
                ->setTitle($faker->sentence($faker->numberBetween(1, 5)))
                ->setDescription(implode(' ', $faker->paragraphs($faker->numberBetween(1, 15))));
            for ($j = 0; $j <= $faker->numberBetween(0, 150); $j++) {
                $task = new Task();
               $status =  $faker->randomElement([
                    StatusesInterface::STATUS_NEW,
                    StatusesInterface::STATUS_DONE,
                    StatusesInterface::STATUS_FAILED,
                    StatusesInterface::STATUS_PENDING,
                ]);
                $task
                    ->setTitle($faker->sentence($faker->numberBetween(1, 5)))
                    ->setDescription(implode(' ', $faker->paragraphs($faker->numberBetween(1, 15))))
                    ->setDuration($faker->dateTimeBetween('-12 months','+36 months'))
                    ->setStatus($status)
                    ->setProject($project);
                $manager->persist($task);

                $project->addTask($task);
            }


            $manager->persist($project);
            if($i % 1000){
                $manager->flush();
                $manager->clear();
            }
        }
        $manager->flush();
    }
}
