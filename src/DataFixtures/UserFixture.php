<?php

namespace App\DataFixtures;

use App\Entity\Project;
use App\Entity\StatusesInterface;
use App\Entity\Task;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixture extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $users[] = $this->createUser('admin','admin@paynetics.local','admin',['ROLE_ADMIN']);
        $users[] = $this->createUser('customer','customer@paynetics.local','customer',['ROLE_CUSTOMER']);

        foreach ($users AS $user){
            $manager->persist($user);
        }
        $manager->flush();

    }

    private function createUser(string $username,string $email,string $password, array $roles = []):User
    {
        $user = new User();
        $user->setEmail($email);
        if(empty($roles)){
            $roles[] = 'USER';
        }
        $user
            ->setRoles($roles)
            ->setPassword( $this->passwordEncoder->encodePassword($user,$password));

        return $user;

    }
}
