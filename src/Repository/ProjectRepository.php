<?php

namespace App\Repository;

use App\Entity\Project;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;

/**
 * @method Project|null find($id, $lockMode = null, $lockVersion = null)
 * @method Project|null findOneBy(array $criteria, array $orderBy = null)
 * @method Project[]    findAll()
 * @method Project[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Project::class);
    }

    public function createApiListQueryBulder(int $page = 0, int $limit=20)
    {
        return $this->createQueryBuilder('p')
            ->select('p')
            ->orderBy('p.id', 'DESC')

           ;

    }
    public function deleteById($id):void
    {
        $object = $this->find($id);
        if(!$object){
            throw new \Exception("Object whit id {$id} not found",400);
        }
        $this->_em->remove($object);
        $this->_em->flush();
    }

    public function store(Project $project): void
    {
        $this->_em->persist($project);
        $this->_em->flush();
    }
}
