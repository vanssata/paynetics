<?php

namespace App\Repository;

use App\Entity\Task;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Task|null find($id, $lockMode = null, $lockVersion = null)
 * @method Task|null findOneBy(array $criteria, array $orderBy = null)
 * @method Task[]    findAll()
 * @method Task[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Task::class);
    }
    /**
     * @return Task[] Returns an array of Projects objects
     */

    public function createApiListQueryBulder()
    {
        return $this->createQueryBuilder('t')
            ->select('t')
            ->orderBy('t.id', 'DESC');

    }

    public function store(Task $task):void
    {
        $this->_em->persist($task);
        $this->_em->flush();
    }

    public function deleteById($id):void
    {
        $object = $this->find($id);
        if(!$object){
            throw new \Exception("Object whit id {$id} not found",400);
        }
        $this->_em->remove($object);
        $this->_em->flush();
    }

    public function delete(Task $task):void
    {
        $this->_em->remove($task);
        $this->_em->flush();
    }
}
