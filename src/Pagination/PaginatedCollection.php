<?php


namespace App\Pagination;


use Symfony\Component\Serializer\Annotation\Groups;

class PaginatedCollection
{
    /**
     * @Groups({"default"})
     */
    private $items;
    /**
     * @Groups({"default"})
     */
    private $total;
    /**
     * @Groups({"default"})
     */
    private $count;
    /**
     * @Groups({"default"})
     */
    private $page;
    /**
     * @Groups({"default"})
     */
    private $total_pages;
    /**
     * @Groups({"default"})
     */
    private $links = array();



    public function __construct(array $items, int $total, int $page, int $total_pages)
    {
        $this->items = $items;
        $this->total = $total;
        $this->count = count($items);
        $this->page = $page;
        $this->total_pages = $total_pages;
    }

    public function addLink($rel,$url){
        $this->links[$rel] = $url;
    }

    public function getLinks():array
    {
        return $this->links;
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getTotalPages(): int
    {
        return $this->total_pages;
    }




}
