<?php
declare(strict_types=1);

namespace App\Pagination;


use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;


class PaginationFactory
{
    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param QueryBuilder $qb
     * @param Request $request
     * @param string $route
     * @param array $routeParams
     * @return PaginatedCollection
     */
    public function createCollectionFromRequest(QueryBuilder $qb, Request $request, string $route, array $routeParams = []): PaginatedCollection
    {
        if (!isset($routeParams['limit'])) {
            $routeParams['limit'] = $request->query->get('limit', 20);
        }

        $pagination = new  Pagerfanta(new QueryAdapter($qb));
        $pagination->setMaxPerPage($routeParams['limit']);
        $pagination->setCurrentPage($request->query->get('page', 1));

        $items = [];
        foreach ($pagination->getCurrentPageResults() as $item) {
            $items[] = $item;
        }

        $paginationCollection = new PaginatedCollection($items, $pagination->getNbResults(),$pagination->getCurrentPage(),$pagination->getNbPages());
        $createLink = function ($targetPage) use ($route, $routeParams) {
            return $this->router->generate($route, array_merge(
                $routeParams,
                ['page' => $targetPage]
            ));
        };

        $paginationCollection->addLink('first', $createLink(1));


        if ($pagination->hasPreviousPage()) {
            $paginationCollection->addLink('prev', $createLink($pagination->getPreviousPage()));
        } else {
            $paginationCollection->addLink('prev', null);
        }

        $paginationCollection->addLink('self', $createLink($pagination->getCurrentPage()));

        if ($pagination->hasNextPage()) {
            $paginationCollection->addLink('next', $createLink($pagination->getNextPage()));
        } else {
            $paginationCollection->addLink('next', null);
        }

        $paginationCollection->addLink('last', $createLink($pagination->getNbPages()));


        return $paginationCollection;

    }
}
