<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210117095038 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE project DROP status');
        $this->addSql('ALTER TABLE project DROP duration');
        $this->addSql('ALTER TABLE task ALTER status TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE task ALTER status DROP DEFAULT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE project ADD status JSON NOT NULL');
        $this->addSql('ALTER TABLE project ADD duration TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE task ALTER status TYPE JSON');
        $this->addSql('ALTER TABLE task ALTER status DROP DEFAULT');
    }
}
