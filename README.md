> # Instalation - use symfony cli and docker
```bash
composer install && \
yarn install && \
yarn build && \
docker-compose up -d --force-recreate && \
symfony serve --allow-http -d && \
symfony console d:m:m && \
symfony console d:f:l
```
